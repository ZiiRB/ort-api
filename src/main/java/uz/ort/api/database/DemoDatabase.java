package uz.ort.api.database;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

public class DemoDatabase extends AbstructDatabase {

    public DemoDatabase(Vertx vertx, JsonObject config) {
        super(vertx, config);
    }
}
